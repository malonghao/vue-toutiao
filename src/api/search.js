import request from '@/utils/request'

// 搜索联想
export const getSearchSuggestion = q => {
  return request({
    method: 'GET',
    url: '/v1_0/suggestion',
    params: {
      q
    }

  })
}

/**
 * 搜索结果
 */
export function getSearch (params) {
  return request({
    method: 'GET',
    url: '/v1_0/search',
    params
  })
}
